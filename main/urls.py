"""main URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from api.models import MovieResource
from . import settings, views

movie_resource = MovieResource()

urlpatterns = [
    path('', views.home),
    path('admin/', admin.site.urls),
    path('movies/', include('movies.urls')),
    path('api/', include(movie_resource.urls)),
]

admin.site.site_header = settings.ADMIN_SITE_TEXT['header']
admin.site.index_title = settings.ADMIN_SITE_TEXT['index_title']
admin.site.site_title = settings.ADMIN_SITE_TEXT['site_title']