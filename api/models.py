from django.db import models
from tastypie.resources import ModelResource
from movies.models import Movie


class MovieResource(ModelResource):
    class Meta:
        resource_name = 'movies'
        queryset = Movie.objects.all()
        excludes = ['date_created']
