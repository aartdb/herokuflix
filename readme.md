# The purpose of this fiddle...
... is simply to get used to working with Python and Django. I learned the basics and more, just for fun. After I found I could make pretty much everything I would want to, I quit this little project and moved on to a project at a client from [Ordina's](https://ordina.nl).

It features:
- [An admin area](https://herokuflix-aart.herokuapp.com/admin)
- [A page which displays movies](https://herokuflix-aart.herokuapp.com/movies)
- A page which displays a single movie (click on any movie which is displayed)
- [An api](https://herokuflix-aart.herokuapp.com/api/movies)

View the website (running on Heroku) at https://herokuflix-aart.herokuapp.com/

